# Sensor Pickle To Octomap

## Compile my\_point\_cloud\_reader.cpp

    ./compile.sh

## Example Work Flow:
    
    ./sensor_pickle_to_xyz_csv.py ./test.pickle > test_xyz.csv
    ./my_point_cloud_reader --xyz_csv ./test_xyz.csv --out test.bt
    octovis test.bt
